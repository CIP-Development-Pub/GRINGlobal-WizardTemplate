﻿using GRINGlobal.Client.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TemplateWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class TemplateForm : Form, IGRINGlobalDataWizard
    {
        public TemplateForm(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }

        public string FormName
        {
            get
            {
                return "Hello World Wizard ";
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }
    }
}
